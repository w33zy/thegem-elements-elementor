<?php
function portfolio_grid_more_callback() {
	$settings = isset($_POST['data']) ? json_decode(stripslashes($_POST['data']), true) : array();
	ob_start();
	$response = array('status' => 'success');
	$page = isset($settings['more_page']) ? intval($settings['more_page']) : 1;
	if ($page == 0)
		$page = 1;
	$portfolio_loop = thegem_get_portfolio_posts($settings['content_portfolios_cat'], $page, $settings['items_per_page'], $settings['orderby'], $settings['order']);
	if ($portfolio_loop->max_num_pages > $page)
		$next_page = $page + 1;
	else
		$next_page = 0;

	$item_classes = get_thegem_portfolio_render_item_classes($settings);
	$thegem_sizes = get_thegem_portfolio_render_item_image_sizes($settings);
	?>
	<div data-page="<?php echo esc_attr($page); ?>" data-next-page="<?php echo esc_attr($next_page); ?>" data-pages-count="<?php echo esc_attr($portfolio_loop->max_num_pages); ?>">
		<?php while ($portfolio_loop->have_posts()) : $portfolio_loop->the_post();
			echo thegem_portfolio_grid_render_item($settings, $item_classes, $thegem_sizes, get_the_ID());
		endwhile; ?>
	</div>
	<?php $response['html'] = trim(preg_replace('/\s\s+/', '', ob_get_clean()));
	$response = json_encode($response);
	header("Content-Type: application/json");
	echo $response;
	exit;
}
add_action('wp_ajax_portfolio_grid_load_more', 'portfolio_grid_more_callback');
add_action('wp_ajax_nopriv_portfolio_grid_load_more', 'portfolio_grid_more_callback');

function thegem_get_portfolio_posts($portfolios_cat, $page = 1, $ppp = -1, $orderby = 'menu_order ID', $order = 'ASC') {
	if (empty($portfolios_cat)) {
		return null;
	}

	$args = array(
		'post_type' => 'thegem_pf_item',
		'post_status' => 'publish',
		'orderby' => $orderby,
		'order' => $order,
		'paged' => $page,
		'posts_per_page' => $ppp,
	);

	if (!in_array('0', $portfolios_cat, true)) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'thegem_portfolios',
				'field' => 'slug',
				'terms' => $portfolios_cat
			)
		);
	}

	$portfolio_loop = new WP_Query($args);

	return $portfolio_loop;
}