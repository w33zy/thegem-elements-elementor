(function($) {
	$(function() {

		let selectedTemplateType = () => {
			let $typeSelect = $('select[name=template_type]');

			let $isHeader = $('.thegem-templates-new-popup .show-is-header');
			$typeSelect.val() === 'header' ? $isHeader.show() : $isHeader.hide();
			$(document).on('change', 'select[name=template_type]', function () {
				$typeSelect.val() === 'header' ? $isHeader.show() : $isHeader.hide();
			});

			let $isFooter = $('.thegem-templates-new-popup .show-is-footer');
			$typeSelect.val() === 'footer' ? $isFooter.show() : $isFooter.hide();
			$(document).on('change', 'select[name=template_type]', function () {
				$typeSelect.val() === 'footer' ? $isFooter.show() : $isFooter.hide();
			});

			let $isTitle = $('.thegem-templates-new-popup .show-is-title');
			$typeSelect.val() === 'title' ? $isTitle.show() : $isTitle.hide();
			$(document).on('change', 'select[name=template_type]', function () {
				$typeSelect.val() === 'title' ? $isTitle.show() : $isTitle.hide();
			});

		}

		let openTemplatesNewPopup = () => {
			$(document).on('click', '.page-title-action:first', function(e) {
				e.preventDefault();
				if($('#thegem-templates-new-popup').length) {
					$.fancybox.open($('#thegem-templates-new-popup').text(), {
						modal: true
					});

					selectedTemplateType();
				}
			});

			selectedTemplateType();
		}

		let openTemplatesImportPopup = () => {
			$(document).on('click', '#thegem-templates-import-link', function(e) {
				e.preventDefault();
				var $button = $(this);
				if($('#thegem-templates-import-popup').length) {
					$.fancybox.close();
					$.fancybox.open($('#thegem-templates-import-popup[data-template-type="'+$(this).data('target-template-type')+'"]').text(), {
						modal: true
					});
				}

				let $grid = $('.thegem-templates-import-grid');
				let $images = $('.template-preview-image img', $grid);
				let length = $images.length;
				$images.each(function(i, el) {
					if( $(el).length && $(el)[0].complete ) {
						length--;
						if( length === 0 ) {
							$grid.removeClass('loading');
						}
					} else {
						$(el).on('load error', function () {
							$grid.removeClass('loading');
						})
					}
				});

				let $filterItem = $('.thegem-templates-import-grid-wrap > .template');
				let $navItem = $('.thegem-templates-import-nav > ul > li');
				$navItem.on('click', 'a', function(e) {
					e.preventDefault();

					$('a', $navItem).removeClass('active');
					$(this).addClass('active');

					$filterItem.hide();
					let current = $(this).data('cat-slug');
					if (current !== '*') {
						$filterItem.filter(`[data-categories=${current}]`).show();
					} else {
						$filterItem.show();
					}
				});
			});
		}

		openTemplatesNewPopup();
		openTemplatesImportPopup();

		let url = new URL(window.location.href);
		if (url.hash === '#open-modal') {
			$(".page-title-action:first").trigger( "click" );
		}
		if (url.hash === '#open-modal-import') {
			$(".page-title-action:first").trigger( "click" );
			openTemplatesImportPopup();
			$("#thegem-templates-import-link").trigger( "click" );
		}

		$(document).on('click', '.thegem-templates-import-popup .thegem-templates-modal-back', function(e) {
			e.preventDefault();
			if($('#thegem-templates-new-popup').length) {
				$.fancybox.close();
				$.fancybox.open($('#thegem-templates-new-popup').text(), {
					modal: true
				});
			}

			selectedTemplateType();
		});

		/*
		$(document).on('click', '.thegem-templates-import-popup .thegem-template-preview-link', function(e) {
			e.preventDefault();
			var previewLink = $(this).attr('href');
			var importLink = $(this).closest('.thegem-temlate').find('.thegem-templates-insert-link').attr('href');
			if($('#thegem-templates-preview-popup').length) {
				$.fancybox.close();
				$.fancybox.open($('#thegem-templates-preview-popup').text(), {
					modal: true,
					afterLoad: function(e) {
						$('.thegem-templates-preview-popup .thegem-templates-import-link').attr('href', importLink);
						$('<iframe src="'+previewLink+'"></iframe>').appendTo($('.thegem-templates-preview-popup .thegem-template-preview'));
					}
				});
			}
		});

		$(document).on('click', '.thegem-templates-preview-popup .thegem-templates-import-back', function(e) {
			e.preventDefault();
			if($('#thegem-templates-import-popup').length) {
				$.fancybox.close();
				$.fancybox.open($('#thegem-templates-import-popup').text(), {
					modal: true
				});
			}
		});
		*/

		$(document).on('click', '.thegem-templates-modal-close', function(e) {
			e.preventDefault();
			$.fancybox.close();
		});

	});
})(jQuery)
