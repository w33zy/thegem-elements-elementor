<?php

function thegem_templates_post_type_init(){
	global $pagenow, $typenow;
	$name = __('TheGem Templates', 'thegem');
	if(is_admin() && 'edit.php' === $pagenow && !empty($_REQUEST['templates_type'])) {
		$types = thegem_templates_types();
		if(!empty($types[$_REQUEST['templates_type']])) {
			$name = $types[$_REQUEST['templates_type']];
		}
	}
	$labels = array(
		'name' => $name,
		'singular_name' => __('Templates', 'thegem'),
		'menu_name' => __('Templates', 'thegem'),
		'name_admin_bar' => __('TheGem Template', 'thegem'),
		'add_new' => __('Add New', 'thegem'),
		'add_new_item' => __('Add New Template', 'thegem'),
		'new_item' => __('New Template', 'thegem'),
		'edit_item' => __('Edit Template', 'thegem'),
		'view_item' => __('View Template', 'thegem'),
		'all_items' => __('All Templates', 'thegem'),
		'search_items' => __('Search Templates', 'thegem'),
		'not_found' => __('No templates found.', 'thegem'),
		'not_found_in_trash' => __('No templates found in Trash.', 'thegem')
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => false,
		'hierarchical' => false,
		'supports' => array('title', 'editor'),
		'show_in_menu' => false,
		'show_in_admin_bar' => true,
		'rewrite' => false
	);

	register_post_type('thegem_templates', $args);
}
add_action('init', 'thegem_templates_post_type_init', 5);

function thegem_templates_title_footer_migrate() {
	$thegem_theme = wp_get_theme(wp_get_theme()->get('Template'));
	if(thegem_get_option('thegem_templates_migrated') || version_compare($thegem_theme->get('Version'), '5.3.0') < 0) return ;
	$titles_list = get_posts(array(
		'post_type' => 'thegem_title',
		'numberposts' => -1,
		'post_status' => 'any'
	));
	foreach ($titles_list as $title) {
		set_post_type($title->ID, 'thegem_templates');
		update_post_meta($title->ID, 'thegem_template_type', 'title');
	}
	$footers_list = get_posts(array(
		'post_type' => 'thegem_footer',
		'numberposts' => -1,
		'post_status' => 'any'
	));
	foreach ($footers_list as $footer) {
		set_post_type($footer->ID, 'thegem_templates');
		update_post_meta($footer->ID, 'thegem_template_type', 'footer');
	}
	$theme_options = get_option('thegem_theme_options');
	$theme_options['thegem_templates_migrated'] = 1;
	update_option('thegem_theme_options', $theme_options);
}
add_action('init', 'thegem_templates_title_footer_migrate', 5);

function thegem_templates_menu() {
	add_submenu_page('thegem-dashboard-welcome',esc_html__('Templates Builder','thegem'), esc_html__('Templates Builder','thegem'), 'edit_theme_options', 'edit.php?post_type=thegem_templates', '', 2);
}
add_action('admin_menu', 'thegem_templates_menu', 50);

function thegem_templates_types($with_content = true) {
	$types = array(
		'header' => __('Header', 'thegem'),
		'title' => __('Title Area', 'thegem'),
		'footer' => __('Footer', 'thegem'),
	);
	if($with_content) {
		$types['content'] = __('Section', 'thegem');
	}
	return apply_filters('thegem_templates_types', $types);
}

function thegem_get_template_type($post_id = '') {
	$post = get_post($post_id);
	$templates_types = thegem_templates_types();
	if($post && get_post_type($post) === 'thegem_templates') {
		$meta = get_post_meta( $post_id, 'thegem_template_type', true );
		if(isset($templates_types[$meta])) {
			return $meta;
		}
	}
	return 'content';
}

function thegem_get_templates($types = '') {
	$args = array(
		'post_type' => 'thegem_templates',
		'post_status' => 'any',
		'orderby' => 'title',
		'order' => 'ASC',
		'posts_per_page' => -1,
	);
	if(!empty($types)) {
		if(is_string($types)) {
			$types = array($types);
		}
		if(is_array($types)) {
			$args['meta_query'] = array(
				array(
					'key' => 'thegem_template_type',
					'value' => $types,
					'compare' => 'in',
				),
			);
			if(in_array('content', $types)) {
				$args['meta_query'] = array_merge($args['meta_query'], array(
					'relation' => 'OR',
					array(
						'key' => 'thegem_template_type',
						'compare' => 'NOT EXISTS',
					),
				));
			}
		}
	}
	$templates_query = new WP_Query;
	return $templates_query->query($args);
}

function thegem_templates_admin_print_tabs( $views ) {
	$current_type = '';
	$active_class = ' nav-tab-active';
	$templates_types = thegem_templates_types();

	if(!empty($_REQUEST['templates_type']) && isset($templates_types[$_REQUEST['templates_type']])) {
		$current_type = $_REQUEST['templates_type'];
		$active_class = '';
	}

	$baseurl = add_query_arg(array('post_type' => 'thegem_templates'), admin_url('edit.php'));

	if ( 1 >= count( $templates_types ) ) {
		return $views;
	}

	?>
	<div id="thegem-templates-tabs-wrapper" class="nav-tab-wrapper">
		<a class="nav-tab<?php echo $active_class; ?>" href="<?php echo $baseurl; ?>">
			<?php _e('All', 'thegem'); ?>
		</a>
		<?php
			foreach($templates_types as $type => $title) {
				$active_class = '';
				if($current_type === $type) {
					$active_class = ' nav-tab-active';
				}
				$type_url = add_query_arg(array('templates_type' => $type), $baseurl );
				echo '<a class="nav-tab'.$active_class.'" href="'.$type_url.'">'.$title.'</a>';
			}
		?>
	</div>
	<?php
	return $views;
}
add_filter( 'views_edit-thegem_templates', 'thegem_templates_admin_print_tabs');

function thegem_templates_admin_columns_headers( $posts_columns ) {
	$offset = 2;
	$posts_columns = array_slice( $posts_columns, 0, $offset, true ) + [
		'thegem_templates_type' => esc_html__( 'Type', 'thegem' ),
	] + array_slice( $posts_columns, $offset, null, true );
	return $posts_columns;
}
add_action( 'manage_thegem_templates_posts_columns', 'thegem_templates_admin_columns_headers' );

function thegem_templates_admin_columns_content( $column_name, $post_id ) {
	if ( 'thegem_templates_type' === $column_name ) {
		$templates_types = thegem_templates_types();
		$type = thegem_get_template_type($post_id);
		$url = add_query_arg(array('post_type' => 'thegem_templates', 'templates_type' => $type), admin_url('edit.php'));
		echo '<a href="'.$url.'">'.$templates_types[$type].'</a>';
	}
}
add_action( 'manage_thegem_templates_posts_custom_column', 'thegem_templates_admin_columns_content', 10, 2 );

function thegem_templates_admin_query_filter_types(WP_Query $query) {
	global $pagenow, $typenow;
	if(!('edit.php' === $pagenow && 'thegem_templates' === $typenow) || ! empty( $query->query_vars['meta_key'] )) {
		return;
	}
	$templates_types = thegem_templates_types();
	$current_type = '';
	if(!empty($_REQUEST['templates_type']) && isset($templates_types[$_REQUEST['templates_type']])) {
		$current_type = $_REQUEST['templates_type'];
	}
	if(empty($current_type)) {
		return;
	}
	if($current_type === 'content') {
		$meta_query[] = array(
			'relation' => 'OR',
			array(
				'key' => 'thegem_template_type',
				'value' => 'content',
				'compare' => 'LIKE',
			),
			array(
				'key' => 'thegem_template_type',
				'compare' => 'NOT EXISTS',
			),
		);
		$query->set('meta_query', $meta_query);
	} else {
		$query->query_vars['meta_key'] = 'thegem_template_type';
		$query->query_vars['meta_value'] = $current_type;
	}
}
add_action( 'parse_query', 'thegem_templates_admin_query_filter_types' );

function thegem_templates_new_init() {
	if ( 'edit-thegem_templates' !== get_current_screen()->id && 'thegem_templates' !== get_current_screen()->id) {
		return;
	}
	if('thegem_templates' === get_current_screen()->id && 'add' === get_current_screen()->action) {
		$redirect_link = add_query_arg(array('post_type' => 'thegem_templates'), admin_url( 'edit.php' )).'#open-modal';
		wp_redirect($redirect_link);
		die;
	}
	add_action( 'admin_head', 'thegem_templates_new_popup');
	add_action( 'admin_enqueue_scripts', 'thegem_templates_new_enqueue_scripts');
}
add_action( 'current_screen', 'thegem_templates_new_init' );

function thegem_import_templates() {
	require_once __DIR__.'/import-data.php';
	$output_templates = array();
	if (!empty($templates)) {
		foreach($templates as $template) {
			if(isset($template['woo']) && defined('WC_PLUGIN_FILE') && $template['woo'] == 2) continue;
			if(isset($template['woo']) && !defined('WC_PLUGIN_FILE') && $template['woo'] == 1) continue;
			$output_templates[$template['id']] = array_merge($template, array(
				'insert' => add_query_arg(array(
					'_wpnonce' => wp_create_nonce( 'thegem_templates_new' ),
					'action' => 'thegem_templates_new',
					'post_type' => 'thegem_templates',
					'template' => $template['id'],
				), admin_url( 'edit.php' ))
			));
			$output_templates[$template['id']]['pic'] = plugin_dir_url( __FILE__ ) . 'assets/img/previews/'.$output_templates[$template['id']]['pic'];
		}
	}
	return $output_templates;
}

function thegem_templates_new_popup() {
	$templates_types = thegem_templates_types();
	$import_templates = thegem_import_templates();
	$categories = array();
	$categories['header'] = array('*' => esc_html__('All', 'thegem'));
	$categories['footer'] = array('*' => esc_html__('All', 'thegem'));
	$categories['title'] = array('*' => esc_html__('All', 'thegem'));
	foreach($import_templates as $key => $template) {
		if(!empty($template['categories']) && is_array($template['categories'])) {
			$categories[$template['type']] = array_merge($categories[$template['type']], $template['categories']);
			$import_templates[$key]['data-cats'] = implode(' ', array_keys($template['categories']));
		}
	}
?>
<script type="text/template" id="thegem-templates-new-popup">
	<div class="thegem-templates-new-popup">
		<div class="thegem-templates-modal-title">
			<a href="javascript:void(0)" class="thegem-templates-modal-logo"><img src="<?= plugin_dir_url( __FILE__ ) . 'assets/img/logo.svg' ?>" alt="logo" /></a>
			<a href="javascript:void(0);" class="thegem-templates-modal-close"></a>
		</div>
		<div class="thegem-templates-new-welcome">
			<div class="thegem-templates-new-welcome-wrap">
				<div class="thegem-templates-new-welcome-info">
					<div class="title"><?php esc_html_e('Templates Builder', 'thegem'); ?></div>
					<div class="text"><?php esc_html_e('Templates help you to create and edit different parts of your website in one place and reuse this parts globally across your site with few clicks.', 'thegem'); ?></div>
				</div>
				<div class="thegem-templates-new-welcome-form">
					<div class="thegem-templates-new-welcome-form-wrap">
						<form id="thegem-templates-new-form" action="<?php esc_url( admin_url( '/edit.php' ) ); ?>">
							<input type="hidden" name="post_type" value="thegem_templates">
							<input type="hidden" name="action" value="thegem_templates_new">
							<input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( 'thegem_templates_new' ); ?>">
							<div class="thegem-templates-new-field">
								<div class="thegem-templates-new-label"><?php esc_html_e('Select Template Type', 'thegem'); ?>:</div>
								<div class="thegem-templates-new-input">
									<select id="thegem-templates-new-type" name="template_type" required>
										<option value="" disabled selected><?php esc_html_e('Select...', 'thegem'); ?></option>
										<?php
										foreach ( $templates_types as $type => $title ) {
											$selected = !empty($_REQUEST['templates_type']) && $_REQUEST['templates_type'] == $type ? ' selected' : '';
											printf( '<option value="%1$s"%2$s>%3$s</option>', esc_attr( $type ), $selected, esc_html( $title ) );
										}
										?>
									</select>
								</div>
							</div>
							<div class="thegem-templates-new-field">
								<div class="thegem-templates-new-label"><?php esc_html_e('Specify Template Name', 'thegem'); ?>:</div>
								<div class="thegem-templates-new-input">
									<input type="text" placeholder="<?php echo esc_attr__( 'Enter Template Name (Optional)', 'thegem' ); ?>" id="thegem-templates-new-name" name="post_data[post_title]">
								</div>
							</div>
							<div class="thegem-templates-new-submit">
								<button class="btn-solid" id="thegem-templates-new-submit" type="submit"><?php echo esc_html__( 'Create Template', 'thegem' ); ?></button>
								<div class="show-is-header"<?php echo (!empty($_REQUEST['templates_type']) && $_REQUEST['templates_type'] == 'header') ? '' : ' style="display: none;"'; ?>>
									<span class="separator">or</span>
									<a id="thegem-templates-import-link" class="btn-solid" href="javascript:void(0);" data-target-template-type="header"><?php echo esc_html__( 'Import Pre-Built Template', 'thegem' ); ?></a>
								</div>
								<div class="show-is-footer"<?php echo (!empty($_REQUEST['templates_type']) && $_REQUEST['templates_type'] == 'footer') ? '' : ' style="display: none;"'; ?>>
									<span class="separator">or</span>
									<a id="thegem-templates-import-link" class="btn-solid" href="javascript:void(0);" data-target-template-type="footer"><?php echo esc_html__( 'Import Pre-Built Template', 'thegem' ); ?></a>
								</div>
								<div class="show-is-title"<?php echo (!empty($_REQUEST['templates_type']) && $_REQUEST['templates_type'] == 'title') ? '' : ' style="display: none;"'; ?>>
									<span class="separator">or</span>
									<a id="thegem-templates-import-link" class="btn-solid" href="javascript:void(0);" data-target-template-type="title"><?php echo esc_html__( 'Import Pre-Built Template', 'thegem' ); ?></a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		<div>
	</div>
</script>
<script type="text/template" id="thegem-templates-import-popup" data-template-type="header">
	<div class="thegem-templates-import-popup">
		<div class="thegem-templates-modal-title">
			<a href="javascript:void(0);" class="thegem-templates-modal-back"><?php esc_html_e('Back', 'thegem'); ?></a>
			<span class="thegem-templates-modal-text"><?php esc_html_e('Select Template to Insert', 'thegem'); ?></span>
			<a href="javascript:void(0);" class="thegem-templates-modal-close"></a>
		</div>
		<div class="thegem-templates-import-grid loading">
			<div class="thegem-templates-import-nav">
				<ul>
					<?php $cat_active = true; foreach($categories['header'] as $key => $category) : ?>
						<li><a<?php echo ($cat_active ? ' class="active"' : ''); ?> href="javascript:void(0)" data-cat-slug="<?php echo esc_attr($key); ?>"><?php echo esc_html($category); ?></a></li>
					<?php  $cat_active = false; endforeach; ?>
				</ul>
			</div>
			<div class="thegem-templates-import-grid-wrap">
				<?php foreach($import_templates as $template) : ?>
					<?php if($template['type'] === 'header') : ?>
						<div class="template"<?php echo (!empty($template['data-cats']) ? ' data-categories="'.$template['data-cats'].'"' : ''); ?>>
							<div class="template-preview">
								<div class="template-preview-image">
									<img src="<?php echo $template['pic']; ?>" alt="#">
								</div>
								<div class="template-preview-actions">
									<a href="<?php echo $template['insert']; ?>" class="thegem-templates-insert-link"><?php esc_html_e('Insert', 'thegem'); ?></a>
									<a href="<?php echo $template['preview']; ?>" class="thegem-template-preview-link" target="_blank"><?php esc_html_e('Preview', 'thegem'); ?></a>
								</div>
							</div>
							<div class="template-info">
								<div class="template-info-title"><?php echo $template['title']; ?></div>
								<?php if(!empty($template['mark'])) : ?>
									<div class="template-info-mark <?php echo $template['mark']; ?>"><?php echo $template['mark']; ?></div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</script>
<script type="text/template" id="thegem-templates-import-popup" data-template-type="footer">
	<div class="thegem-templates-import-popup">
		<div class="thegem-templates-modal-title">
			<a href="javascript:void(0);" class="thegem-templates-modal-back"><?php esc_html_e('Back', 'thegem'); ?></a>
			<span class="thegem-templates-modal-text"><?php esc_html_e('Select Template to Insert', 'thegem'); ?></span>
			<a href="javascript:void(0);" class="thegem-templates-modal-close"></a>
		</div>
		<div class="thegem-templates-import-grid loading">
			<?php /* 			<div class="thegem-templates-import-nav">
				<ul>
					<?php $cat_active = true; foreach($categories['footer'] as $key => $category) : ?>
						<li><a<?php echo ($cat_active ? ' class="active"' : ''); ?> href="javascript:void(0)" data-cat-slug="<?php echo esc_attr($key); ?>"><?php echo esc_html($category); ?></a></li>
					<?php  $cat_active = false; endforeach; ?>
				</ul>
			</div> */ ?>
			<div class="thegem-templates-import-grid-wrap grid">
				<?php foreach($import_templates as $template) : ?>
					<?php if($template['type'] === 'footer') : ?>
						<div class="template"<?php echo (!empty($template['data-cats']) ? ' data-categories="'.$template['data-cats'].'"' : ''); ?>>
							<div class="template-preview">
								<div class="template-preview-image">
									<img src="<?php echo $template['pic']; ?>" alt="#">
								</div>
								<div class="template-preview-actions">
									<a href="<?php echo $template['insert']; ?>" class="thegem-templates-insert-link"><?php esc_html_e('Insert', 'thegem'); ?></a>
									<a href="<?php echo $template['preview']; ?>" class="thegem-template-preview-link" target="_blank"><?php esc_html_e('Preview', 'thegem'); ?></a>
								</div>
							</div>
							<div class="template-info">
								<div class="template-info-title"><?php echo $template['title']; ?></div>
								<?php if(!empty($template['mark'])) : ?>
									<div class="template-info-mark <?php echo $template['mark']; ?>"><?php echo $template['mark']; ?></div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</script>
<script type="text/template" id="thegem-templates-import-popup" data-template-type="title">
	<div class="thegem-templates-import-popup">
		<div class="thegem-templates-modal-title">
			<a href="javascript:void(0);" class="thegem-templates-modal-back"><?php esc_html_e('Back', 'thegem'); ?></a>
			<span class="thegem-templates-modal-text"><?php esc_html_e('Select Template to Insert', 'thegem'); ?></span>
			<a href="javascript:void(0);" class="thegem-templates-modal-close"></a>
		</div>
		<div class="thegem-templates-import-grid loading">
			<?php /* <div class="thegem-templates-import-nav">
				<ul>
					<?php $cat_active = true; foreach($categories['title'] as $key => $category) : ?>
						<li><a<?php echo ($cat_active ? ' class="active"' : ''); ?> href="javascript:void(0)" data-cat-slug="<?php echo esc_attr($key); ?>"><?php echo esc_html($category); ?></a></li>
					<?php  $cat_active = false; endforeach; ?>
				</ul>
			</div> */ ?>
			<div class="thegem-templates-import-grid-wrap grid">
				<?php foreach($import_templates as $template) : ?>
					<?php if($template['type'] === 'title') : ?>
						<div class="template"<?php echo (!empty($template['data-cats']) ? ' data-categories="'.$template['data-cats'].'"' : ''); ?>>
							<div class="template-preview">
								<div class="template-preview-image">
									<img src="<?php echo $template['pic']; ?>" alt="#">
								</div>
								<div class="template-preview-actions">
									<a href="<?php echo $template['insert']; ?>" class="thegem-templates-insert-link"><?php esc_html_e('Insert', 'thegem'); ?></a>
									<a href="<?php echo $template['preview']; ?>" class="thegem-template-preview-link" target="_blank"><?php esc_html_e('Preview', 'thegem'); ?></a>
								</div>
							</div>
							<div class="template-info">
								<div class="template-info-title"><?php echo $template['title']; ?></div>
								<?php if(!empty($template['mark'])) : ?>
									<div class="template-info-mark <?php echo $template['mark']; ?>"><?php echo $template['mark']; ?></div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</script>
<?php /*<script type="text/template" id="thegem-templates-preview-popup">
	<div class="thegem-templates-preview-popup">
		<div class="thegem-templates-preview-title">
			<a href="javascript:void(0);" class="thegem-templates-import-back"><?php esc_html_e('Back', 'thegem'); ?></a>
			<a href="#" class="thegem-templates-import-link"><?php esc_html_e('Import Template', 'thegem'); ?></a>
			<a href="javascript:void(0);" class="thegem-templates-new-close"></a>
		</div>
		<div class="thegem-template-preview">

		</div>
	</div>
</script> */ ?>
<?php
}

function thegem_templates_new_create() {
		check_admin_referer( 'thegem_templates_new' );
		if(empty($_GET['post_type']) || $_GET['post_type'] !== 'thegem_templates') {
			return;
		}

		$post_type_object = get_post_type_object( 'thegem_templates' );
		if(!current_user_can( $post_type_object->cap->edit_posts)) {
			return;
		}

		$templates_types = thegem_templates_types();
		if ( empty( $_GET['template_type'] ) || !isset($templates_types[sanitize_text_field($_GET['template_type'])])) {
			$type = 'content';
		} else {
			$type = sanitize_text_field( $_GET['template_type'] );
		}

		$post_data = isset( $_GET['post_data'] ) ? $_GET['post_data'] : [];
		$post_data['post_type'] = 'thegem_templates';
		if(empty($post_data['post_title'])) {
			$post_data['post_title'] = __('Draft Template', 'thegem');
		}

		$content = '';
		if(!empty($_GET['template'])) {
			$import_templates = thegem_import_templates();
			if(!empty($import_templates[$_GET['template']])) {
				$template = $import_templates[$_GET['template']];
				$type = $template['type'];
				$post_data['post_title'] = $template['title'];
				$content = wp_slash(thegem_template_import_content($template['content'], 'on_import'));
				if($type === 'header') {
					thegem_templates_import_menus();
				}
			}
		}

		$meta_data = [];
		$meta_data[ 'thegem_template_type' ] = $type;
		if(!empty($content)) {
			$meta_data[ '_elementor_data' ] = $content;
			$meta_data[ '_elementor_edit_mode' ] = 'builder';
		}
		$post_data['meta_input'] = $meta_data;

		$template_id = wp_insert_post( $post_data );
		$redirect_link = add_query_arg(array('post' => $template_id, 'action' => 'edit'), admin_url( 'post.php' ));
		if(defined('ELEMENTOR_VERSION')) {
			$redirect_link = add_query_arg(array('post' => $template_id, 'action' => 'elementor'), admin_url( 'post.php' ));
		}
		wp_redirect($redirect_link);

		die;
}
add_action( 'admin_action_thegem_templates_new', 'thegem_templates_new_create' );

function thegem_template_import_content($content, $method) {
	$obj = json_decode($content, true);
	$response_p = wp_remote_get(add_query_arg(array('code' => thegem_get_purchase(), 'info'=>thegem_get_activation_info(), 'site_url' => get_site_url(), 'type' => 'elementor'), 'http://democontent.codex-themes.com/av_validate_code'.(defined('ENVATO_HOSTED_SITE') ? '_envato' : '').'.php'), array('timeout' => 20));
	if (defined('ELEMENTOR_VERSION') && $obj) {
		$obj = \Elementor\Plugin::$instance->db->iterate_data(
			$obj, function( $element_data ) use ( $method ) {
				$element = \Elementor\Plugin::$instance->elements_manager->create_element_instance( $element_data );
				if ( ! $element ) {
					return null;
				}
				return thegem_template_element_import_content( $element, $method );
			}
		);
		$content = json_encode($obj);
	}

	return $content;
}

function thegem_template_element_import_content( $element, $method ) {
	$element_data = $element->get_data();
	if ( method_exists( $element, $method ) ) {
		$element_data = $element->{$method}( $element_data );
	}
	foreach ( $element->get_controls() as $control ) {
		$control_class = \Elementor\Plugin::$instance->controls_manager->get_control( $control['type'] );
		if ( ! $control_class ) {
			return $element_data;
		}
		if ( method_exists( $control_class, $method ) ) {
			$element_data['settings'][ $control['name'] ] = $control_class->{$method}( $element->get_settings( $control['name'] ), $control );
		}
		if ( 'on_export' === $method && isset( $control['export'] ) && false === $control['export'] ) {
			unset( $element_data['settings'][ $control['name'] ] );
		}
	}
	return $element_data;
}

function thegem_templates_import_menus() {
	$demo_menu = wp_update_nav_menu_object(0, array('menu-name' => 'TheGem-Menu(Demo)'));
	if(!is_wp_error($demo_menu)) {
		$demo_menu_titles = array('Demos', 'Features', 'Elements', 'Pages', 'Shop', 'Blog', 'Portfolio');
		foreach($demo_menu_titles as $title) {
			wp_update_nav_menu_item($demo_menu, 0, array(
				'menu-item-title' => $title,
				'menu-item-url' => '#',
				'menu-item-status' => 'publish',
			));
		}
	}
	$demo_secondary_menu = wp_update_nav_menu_object(0, array('menu-name' => 'TheGem-Secondary Menu (Demo)'));
	if(!is_wp_error($demo_secondary_menu)) {
		$demo_secondary_menu_titles = array('My account', 'FAQ', 'Contact Us', 'Newsletter Signup', 'Shipping Terms');
		foreach($demo_secondary_menu_titles as $title) {
			wp_update_nav_menu_item($demo_secondary_menu, 0, array(
				'menu-item-title' => $title,
				'menu-item-url' => '#',
				'menu-item-status' => 'publish',
			));
		}
	}
	$demo_mini_menu = wp_update_nav_menu_object(0, array('menu-name' => 'TheGem-Menu-Mini(Demo)'));
	if(!is_wp_error($demo_mini_menu)) {
		$demo_mini_menu_titles = array('Demos', 'Elements', 'Pages', 'Shop', 'Blog');
		foreach($demo_mini_menu_titles as $title) {
			wp_update_nav_menu_item($demo_mini_menu, 0, array(
				'menu-item-title' => $title,
				'menu-item-url' => '#',
				'menu-item-status' => 'publish',
			));
		}
	}
}

function thegem_templates_new_enqueue_scripts() {
	wp_enqueue_script('thegem-templates-new', plugin_dir_url(__FILE__). '/assets/js/new-template.js', array('jquery', 'jquery-fancybox'));
}

function thegem_force_header_type_private($post) {
	if ($post['post_type'] == 'thegem_templates' && $post['post_status'] != 'trash') {
		$post['post_status'] = 'private';
	}
	return $post;
}
add_filter('wp_insert_post_data', 'thegem_force_header_type_private');

function thegem_templates_widgets_init() {
	if ( ! did_action( 'elementor/loaded' ) ) return;
//	if ( get_post_type() !== 'thegem_templates' ) return;
	foreach ( glob( __DIR__.'/elements/*/element.php' ) as $filename ) {
		if ( empty( $filename ) || ! is_readable( $filename ) ) {
			continue;
		}
		require $filename;
	}
}
add_action('elementor/widgets/widgets_registered', 'thegem_templates_widgets_init', 5);

function thegem_add_section_settings($obj, $args) {
	if (get_post_type() !== 'thegem_templates' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) !== 'header' ) return;

	$obj->add_control(
		'thegem_row_sticky',
		[
			'label' => __('Header Sticky Section', 'elementor'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'separator' => 'before',
			'default' => '',
			'label_on' => 'Yes',
			'label_off' => 'No',
			'return_value' => 'yes',
			'description' => __('Enable to make this header section sticky while scrolling', 'thegem'),
		]
	);
}
add_action('elementor/element/section/section_layout/before_section_end', 'thegem_add_section_settings', 10, 2);

function thegem_elementor_background_transition_default($obj, $args) {
	if (get_post_type() !== 'thegem_templates' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) !== 'title' ) return;

	$obj->update_control(
		'background_hover_transition',
		[
			'default' => [
				'size' => '',
			],
		]
	);
}
add_action('elementor/element/section/section_background/after_section_end', 'thegem_elementor_background_transition_default', 10, 2);

function thegem_section_add_sticky_class($obj) {
	if (get_post_type() !== 'thegem_templates' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) !== 'header' || 'section' !== $obj->get_name()) {
		return;
	}

	$settings = $obj->get_settings_for_display();

	if (!empty($settings['thegem_row_sticky'])) {
		$obj->add_render_attribute(
			'_wrapper', 'class', 'header-sticky-row'
		);
	}
}
add_action('elementor/frontend/section/before_render', 'thegem_section_add_sticky_class');

function thegem_add_column_settings_inline($obj, $args) {
	if (get_post_type() !== 'thegem_templates' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) !== 'header') return;

	$obj->add_control(
		'thegem_inline_elements',
		[
			'label' => __( 'Place Elements Inline', 'elementor' ),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'default' => 'yes',
			'label_on' => 'Yes',
			'label_off' => 'No',
			'return_value' => 'yes',
		]
	);

	$wrap_device_args = [
		'desktop' => [
			'options' => [
				'' => __('Default', 'thegem'),
				'nowrap' => __('Nowrap', 'thegem'),
				'wrap' => __('Wrap', 'thegem'),
				'wrap-reverse' => __('Wrap Reverse', 'thegem'),
			],
		],
		'tablet' => [
			'options' => [
				'' => __('Inherit from Desktop', 'thegem'),
				'nowrap' => __('Nowrap', 'thegem'),
				'wrap' => __('Wrap', 'thegem'),
				'wrap-reverse' => __('Wrap Reverse', 'thegem'),
			],
		],
		'mobile' => [
			'options' => [
				'' => __('Inherit from Tablet', 'thegem'),
				'nowrap' => __('Nowrap', 'thegem'),
				'wrap' => __('Wrap', 'thegem'),
				'wrap-reverse' => __('Wrap Reverse', 'thegem'),
			],
		]
	];
	$obj->add_responsive_control(
		'thegem_items_wrap',
		[
			'label' => __( 'Items Wrap', 'elementor' ),
			'type' => \Elementor\Controls_Manager::SELECT,
			'default' => 'wrap',
			'tablet_default' => '',
			'mobile_default' => '',
			'device_args' => $wrap_device_args,
			'selectors' => [
				'{{WRAPPER}} .elementor-widget-wrap' => 'flex-wrap: {{VALUE}};',
			],
		]
	);
}
add_action('elementor/element/column/layout/before_section_end', 'thegem_add_column_settings_inline', 10, 2);

function thegem_column_add_inline_class($obj) {
	if (get_post_type() !== 'thegem_templates' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) !== 'header' || 'column' !== $obj->get_name() || isset($GLOBALS['thegem_menu_template'])) return;

	$settings = $obj->get_settings_for_display();

	if ( !isset( $settings['thegem_inline_elements']) || $settings['thegem_inline_elements'] == 'yes' ) {
		$obj->add_render_attribute(
			'_widget_wrapper', 'class', 'thegem-column-elements-inline'
		);
	}
}
add_action('elementor/frontend/column/before_render', 'thegem_column_add_inline_class');

function thegem_column_change_template_inline($template, $obj) {
	if ( get_post_type() !== 'thegem_templates' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) !== 'header') return $template;
	if('column' === $obj->get_name()) {
		$old_template = '<div class="elementor-widget-wrap"></div>';
		ob_start();
?>
<#
view.addRenderAttribute( 'block_classes', 'class', 'elementor-widget-wrap' );
if ( 'yes' === settings.thegem_inline_elements) {
	view.addRenderAttribute( 'block_classes', 'class', 'thegem-column-elements-inline' );
}
var elementor_widget_wrap = '<div ' + view.getRenderAttributeString( 'block_classes' ) + '></div>';
print( elementor_widget_wrap );
#>
<?php
		$new_template = ob_get_clean();

		$template = str_replace( $old_template, $new_template, $template );
	}

	return $template;
}
add_filter( 'elementor/column/print_template', 'thegem_column_change_template_inline', 10, 2);

function add_thegem_flex_section($element, $section_id, $args) {
	if ((is_admin() && (get_post_type() !== 'thegem_templates' || thegem_get_template_type(get_the_id()) != 'header')) || $section_id !== '_section_style') return;

	$element->start_controls_section(
		'flex_section',
		[
			'label' => __('Flex Options', 'thegem'),
			'tab' => \Elementor\Controls_Manager::TAB_CONTENT
		]
	);

	$return_value_device_args = [
		'desktop' => [
			'return_value' => 'desktop',
		],
		'tablet' => [
			'return_value' => 'tablet',
		],
		'mobile' => [
			'return_value' => 'mobile',
		]
	];

	if (version_compare(ELEMENTOR_VERSION, '3.4.0', '>=')) {
		$return_value_device_args_hide = $return_value_device_args;
	} else {
		$return_value_device_args_hide = [
			'desktop' => [
				'return_value' => 'desktop',
			],
			'tablet' => [
				'return_value' => 'tablet',
			],
			'mobile' => [
				'return_value' => 'phone',
			]
		];
	}

	$element->add_responsive_control(
		'flex_hide_element',
		[
			'label' => __('Hide Element', 'thegem'),
			'default' => '',
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'render_type' => 'template',
			'label_on' => __('On', 'thegem'),
			'label_off' => __('Off', 'thegem'),
			'prefix_class' => 'elementor-hidden-',
			'device_args' => $return_value_device_args_hide,
			'description' => __('Responsive visibility will take effect only on preview or live page, and not while editing in Elementor', 'thegem'),
		]
	);

	$element->add_responsive_control(
		'flex_sort_order',
		[
			'label' => __('Appearance Order', 'thegem'),
			'type' => \Elementor\Controls_Manager::NUMBER,
			//'devices' => ['tablet', 'mobile'],
			'render_type' => 'template',
			'step' => 1,
			'description' => __('Appearance order of the header element in the column', 'thegem'),
			'selectors' => [
				'.thegem-template-header {{WRAPPER}}' => 'order: {{VALUE}};',
			],
		]
	);

	$element->add_responsive_control(
		'flex_absolute',
		[
			'label' => __('Position Absolute', 'thegem'),
			'default' => '',
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'render_type' => 'template',
			'label_on' => __('On', 'thegem'),
			'label_off' => __('Off', 'thegem'),
			'prefix_class' => 'flex-absolute-',
			'device_args' => $return_value_device_args,
		]
	);

	$horizontal_align_device_args = [
		'desktop' => [
			'options' => [
				'default' => __('Default', 'thegem'),
				'left' => __('Left', 'thegem'),
				'center' => __('Center', 'thegem'),
				'right' => __('Right', 'thegem'),
			],
		],
		'tablet' => [
			'options' => [
				'default' => __('Inherit from Desktop', 'thegem'),
				'unset' => __('Unset (not inherited)', 'thegem'),
				'left' => __('Left', 'thegem'),
				'center' => __('Center', 'thegem'),
				'right' => __('Right', 'thegem'),
			],
		],
		'mobile' => [
			'options' => [
				'default' => __('Inherit from Tablet', 'thegem'),
				'unset' => __('Unset (not inherited)', 'thegem'),
				'left' => __('Left', 'thegem'),
				'center' => __('Center', 'thegem'),
				'right' => __('Right', 'thegem'),
			],
		]
	];

	$element->add_responsive_control(
		'flex_horizontal_align_relative',
		[
			'label' => __('Horizontal Align', 'thegem'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'devices' => ['desktop', 'tablet', 'mobile'],
			'default' => 'default',
			'tablet_default' => 'default',
			'mobile_default' => 'default',
			'device_args' => $horizontal_align_device_args,
			'prefix_class' => 'flex-horizontal-align%s-',
			'description' => __('Horizontal align of header element. Works only if "Place elements inline" setting is activated in the column.', 'thegem'),
		]
	);

	$vertical_align_device_args = [
		'desktop' => [
			'options' => [
				'default' => __('Default', 'thegem'),
				'top' => __('Top', 'thegem'),
				'center' => __('Middle', 'thegem'),
				'bottom' => __('Bottom', 'thegem'),
			],
		],
		'tablet' => [
			'options' => [
				'default' => __('Inherit from Desktop', 'thegem'),
				'unset' => __('Unset (not inherited)', 'thegem'),
				'top' => __('Top', 'thegem'),
				'center' => __('Middle', 'thegem'),
				'bottom' => __('Bottom', 'thegem'),
			],
		],
		'mobile' => [
			'options' => [
				'default' => __('Inherit from Tablet', 'thegem'),
				'unset' => __('Unset (not inherited)', 'thegem'),
				'top' => __('Top', 'thegem'),
				'center' => __('Middle', 'thegem'),
				'bottom' => __('Bottom', 'thegem'),
			],
		]
	];

	$element->add_responsive_control(
		'flex_vertical_align_relative',
		[
			'label' => __('Vertical Align', 'thegem'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'devices' => ['desktop', 'tablet', 'mobile'],
			'default' => 'default',
			'tablet_default' => 'default',
			'mobile_default' => 'default',
			'device_args' => $vertical_align_device_args,
			'prefix_class' => 'flex-vertical-align%s-',
			'description' => __('Vertical align of header element. Works only if "Place elements inline" setting is activated in the column.', 'thegem'),
		]
	);

	$element->add_responsive_control(
		'flex_padding',
		[
			'label' => __('Flex Padding', 'thegem'),
			'type' => \Elementor\Controls_Manager::DIMENSIONS,
			'render_type' => 'template',
			'size_units' => ['px', '%', 'rem', 'em'],
			'default' => [
				'top' => 0,
				'right' => 5,
				'bottom' => 0,
				'left' => 5,
				'unit' => 'px',
			],
			'description' => __('Unlike padding in advanced tab, this setting applies to the flex container of the header element.', 'thegem'),
			'selectors' => [
				'.thegem-template-header {{WRAPPER}}' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			],
		]
	);

	$element->add_responsive_control(
		'flex_margin',
		[
			'label' => __('Flex Margin', 'thegem'),
			'type' => \Elementor\Controls_Manager::DIMENSIONS,
			'render_type' => 'template',
			'size_units' => ['px', '%', 'rem', 'em'],
			'description' => __('Unlike margin in advanced tab, this setting applies to the flex container of the header element.', 'thegem'),
			'selectors' => [
				'.thegem-template-header {{WRAPPER}}' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			],
		]
	);

	$element->end_controls_section();
}
add_action('elementor/element/before_section_start', 'add_thegem_flex_section', 10, 3);


add_action( 'elementor/element/social-icons/section_social_icon/before_section_end', function( $element, $args ) {

	if (get_post_type() !== 'thegem_templates' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) !== 'header') return;

	$element->add_responsive_control (
		'icons_size',
		[
			'label' => __('Size Preset', 'thegem'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'default' => 'tiny',
			'options' => [
				'' => __('Select Size', 'thegem'),
				'tiny' => __('Tiny', 'thegem'),
				'small' => __('Small', 'thegem'),
				'medium' => __('Medium', 'thegem'),
				'large' => __('Large', 'thegem'),
				'xlarge' => __('Extra Large', 'thegem'),
			],
			'selectors_dictionary' => [
				'tiny' => '16px',
				'small' => '24px',
				'medium' => '48px',
				'large' => '96px',
				'xlarge' => '144px',
			],
			'selectors' => [
				'{{WRAPPER}}' => '--icon-size: {{VALUE}}',
			],
		]
	);
}, 10, 2 );

add_action( 'elementor/element/social-icons/section_social_style/before_section_end', function( $element, $args ) {

	if (get_post_type() !== 'thegem_templates' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) !== 'header') return;

	$element->update_control(
		'icon_padding',
		[
			'default' => [
				'size' => 0,
			],
		]
	);

	$element->update_control(
		'icon_spacing',
		[
			'default' => [
				'size' => 20,
			],
		]
	);

	$element->update_control(
		'shape',
		[
			'default' => 'simple',
			'options' => [
				'simple' => esc_html__( 'Simple Shape', 'elementor' ),
				'rounded' => esc_html__( 'Rounded', 'elementor' ),
				'square' => esc_html__( 'Square', 'elementor' ),
				'circle' => esc_html__( 'Circle', 'elementor' ),
			],
		]
	);
}, 10, 2 );

function thegem_te_delay_class() {
	if(function_exists('thegem_is_wp_rocket_delay_js_active') && thegem_is_wp_rocket_delay_js_active()) {
		return ' detect-delay-click';
	}
	return '';
}

function thegem_deactivate_elementor_header_footer() {
	if (get_post_type() === 'thegem_templates' && (get_post_meta( get_the_ID(), 'thegem_template_type', true ) === 'header' || get_post_meta( get_the_ID(), 'thegem_template_type', true ) === 'footer')) {
		remove_action( 'get_header', [ \ElementorPro\Modules\ThemeBuilder\Module::instance()->get_component( 'theme_support' ), 'get_header' ] );
		remove_action( 'get_footer', [ \ElementorPro\Modules\ThemeBuilder\Module::instance()->get_component( 'theme_support' ), 'get_footer' ] );
	}
}
add_action( 'elementor/theme/register_locations', 'thegem_deactivate_elementor_header_footer', 100 );

function thegem_elementor_theme_templates_popup() {
	if ( 'edit-thegem_templates' !== get_current_screen()->id && 'thegem_templates' !== get_current_screen()->id) return;
	if(!defined('ELEMENTOR_PRO_VERSION')) return ;
	if(!empty( $_COOKIE['thegem_elementor_theme_templates'] )) return ;
	$headers = \Elementor\Plugin::$instance->templates_manager->get_source( 'local' )->get_items(array('type' => 'header'));
	$footers = \Elementor\Plugin::$instance->templates_manager->get_source( 'local' )->get_items(array('type' => 'footer'));
	if(empty($headers) && empty($footers)) return ;
	wp_enqueue_style('thegem-activation-google-fonts');
?>
<script type="text/javascript">
(function ( $ ) {
	var setCookie = function ( c_name, value, exdays ) {
		var exdate = new Date();
		exdate.setDate( exdate.getDate() + exdays );
		var c_value = encodeURIComponent( value ) + ((null === exdays) ? "" : "; expires=" + exdate.toUTCString());
		document.cookie = c_name + "=" + c_value;
	};
	$( document ).on( 'click.thegem-notice-dismiss', '#thegem_elementor_theme_templates-popup .thegem-notice-dismiss', function ( e ) {
		e.preventDefault();
		$.fancybox.close();
		setCookie( 'thegem_elementor_theme_templates', '1', 30 );
	} );
	thegem_show_elementor_conflict_templates_popup = function() {
		var $popupContent = $('#thegem_elementor_theme_templates-popup');
		if($popupContent.length) {
			$.fancybox.open({
				src : '#thegem_elementor_theme_templates-popup',
				type : 'inline',
				modal: true
			});
		}
	}
	$(function() {
		thegem_show_elementor_conflict_templates_popup();
	})
})( window.jQuery );
</script>
<style>
#thegem_elementor_theme_templates-popup {
	width: 550px;
	text-align: center;
}
#thegem_elementor_theme_templates-popup p {
	font-family: "Source Sans Pro";
	font-weight: normal;
	font-size: 16px;
	line-height: 25px;
	margin-bottom: 30px;
	color: #5f727f;
}
#thegem_elementor_theme_templates-popup button {
	border: none;
	padding: 5px 15px;
	border-radius: 3px;
	font-size: 14px;
	font-weight: 400;
	font-family: 'Montserrat';
	text-transform: uppercase;
	text-align: center;
	min-width: 150px;
	margin: 0 12px;
	text-decoration: none;
	display: inline-block;
	vertical-align: middle;
	box-sizing: border-box;
	box-shadow: none;
	transition: all 0.3s linear;
	-moz-transition: all 0.3s linear;
	-webkit-transition: all 0.3s linear;
	cursor: pointer;
	background-color: #00bcd4;
	color: #fff;
	outline: 0 none;
}
#thegem_elementor_theme_templates-popup button:hover {
	background-color: #3c3950;
	color: #ffffff;
}
</style>
<?php
	echo '<div id="thegem_elementor_theme_templates-popup" style="display: none;"><p>' . wp_kses(__( 'TheGem has detected header and/or footer templates built with Elementor Pro on your website.<br>Please note: in case any of this Elementor Pro templates are activated for your website, they will override TheGem templates.<br>In order to use headers and/or footers built with TheGem please make sure that no Elementor Pro headers or footers are activated.', 'thegem' ), array('br' => array()) ) . '</p>' . '<button type="button" class="thegem-notice-dismiss button-primary">' . __( 'Dismiss this notice', 'default' ) . '</button></div>';
}
add_action('admin_footer', 'thegem_elementor_theme_templates_popup');

function thegem_get_section_templates_list() {
	$templates = array();
	$templates_list = thegem_get_templates('content');
	foreach ($templates_list as $template) {
		$templates[$template->ID] = $template->post_title . ' (ID = ' . $template->ID . ')';
	}
	return $templates;
}